# Scraping all issues within an issue to output a list of issues that have due dates
# Currently it is hard coded to only report on open issues
# It will add a confidence indicator if the description has a pattern of Confidence ##% or ##% Confidence
# A file called `epic_status.txt` is created
# Example command line ruby epic_status.rb --epic 5759

require 'date'
require_relative 'get_epic_issues'
require 'pp'
require 'docopt'
require 'logger'
require 'gitlab'
require 'dotenv/load'
require_relative 'modules'


docstring = <<DOCSTRING
Create Issue and Merge Request burn up charts for the given epics.

Usage:
  #{__FILE__} --epic=<epic1,epic2,epic3> [--group=<9970>] [--comment=<true|false>]
  #{__FILE__} -h | --help

Options:
  -h --help                 Show this screen.
  --epic=<epic>             List the epics you would like to display in burn up charts 
  --group=<n>               The group where the epic resides [default: 9970]
  --comment=<true|false>    If true, then a comment will be added to the epic id passed in [default: false]           
DOCSTRING

options = Docopt::docopt(docstring)
epics = options.fetch('--epic')
comment = options.fetch('--comment')=='true'
group = options.fetch('--group')
filepath = "epic_status.txt"
epic_hash = Hash.new

Gitlab.configure do |config|
  config.endpoint = 'https://gitlab.com/api/v4'
  #puts 'Enter your private token'
  config.private_token = ENV["PAT"]
end

def put_confidence_level(fileio, description)
  confidence = description.match("([c|C]onfidence.*([0-9]+)%)|(([0-9]+)%.*[c|C]onfidence)")
      if confidence
          fileio.puts("    - #{confidence}")
      end
end

epics_arr = []
child_epic_ids = child_epics = get_child_epics(epics.split(','), group)
child_epic_ids.each do |id|
  epics_arr << Gitlab.epics(id) 
end


f_status = File.open(filepath, "w")

epics_arr.sort_by{|i| i.due_date.to_s}.each do |epic|
  
  all_issues = get_all_issues(epic.iid.to_s.split(','))
  if all_issues.select{|i| i.state=='opened'}.count > 0
    f_status.puts("- **Epic**: #{epic.title} - #{epic.web_url}")
    f_status.puts("    - Due: #{epic.due_date}")
    put_confidence_level(f_status, epic.description)
    
    issues_due = all_issues.select{|i| !i.due_date.nil?}.select{|i| i.state=='opened'}.sort_by{|i| i.due_date}
        
    issues_due.each do |issue|
        f_status.puts("  - **Issue**: #{issue.title} - #{issue.web_url}")
        f_status.puts("    - Due: #{issue.due_date}")
        f_status.puts("    - State: #{issue.state}")

        put_confidence_level(f_status, issue.description)
        
        issue.assignees.each do |assignee|
            f_status.puts("    - Who: #{assignee.name}")
        end 
        
    end
  end
end
f_status.close
if comment
  epics.split(',').each do |epic|
   epic_info = epic_hash[epic.to_s]
   pp epic_hash 
   logger.info("epic_info #{epic_info} epic #{epic}")
   logger.info("Adding comment for epic id #{epic} iid #{epic_info.iid}")
   text = File.open(filepath)
   Gitlab.create_epic_note(group, epic_info.id, text.read)
  end
end

=begin
all_issues = get_all_issues(get_child_epics(epics.split(',')))
issues_due = all_issues.select{|i| !i.due_date.nil?}.select{|i| i.state=='opened'}.sort_by{|i| i.due_date}
f_status = File.open("epic_status.txt", "w")
    
issues_due.each do |issue|
    f_status.puts("- #{issue.title} - #{issue.web_url}")
    f_status.puts("  - Due: #{issue.due_date}")
    f_status.puts("  - State: #{issue.state}")
    confidence = issue.description.match("([c|C]onfidence.*([0-9]+)%)|(([0-9]+)%.*[c|C]onfidence)")
    if confidence
        f_status.puts("  - #{confidence}")
    end
    issue.assignees.each do |assignee|
        f_status.puts("  - Who: #{assignee.name}")
    end 
    
end
=end 