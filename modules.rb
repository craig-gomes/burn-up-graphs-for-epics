
class Gitlab::Client
    
  module Epics
  
      def epics(epic_iid, group=9970)
        get("/groups/#{group}/epics/#{epic_iid}")
      end
  
      def comment(epic_iid, body, group=9970)
        put("/groups/#{group}/epics/#{epic_iid}/notes?body=#{body}")
      end
      
      def epic_children(epic_iid, group=9970)
        get("/groups/#{group}/epics/#{epic_iid}/epics?per_page=#{PER_PAGE}")
      end
  
      def all_epic_issues(epic_iid, group=9970)
        should_continue=true
        current_page = 1
        all_issues = []
        while should_continue
          
          if !should_continue
            break
          end
          pp "/groups/#{group}/epics/#{epic_iid}/issues?per_page=#{PER_PAGE}&page=#{current_page}"
          issues = get("/groups/#{group}/epics/#{epic_iid}/issues?per_page=#{PER_PAGE}&page=#{current_page}")
          if issues.size < PER_PAGE
            should_continue=false
          end
          all_issues += issues
          current_page +=1
          
        end
        all_issues
        
      end
  
    end

    module MergeRequests
      def related(project_id, issue_id)
        should_continue=true
        all_mrs=[]

        (1..100).each do |current_page|
          if !should_continue
            break
          end

          mrs = get("/projects/#{project_id}/issues/#{issue_id}/related_merge_requests", {per_page:PER_PAGE})
          
          if mrs.size < PER_PAGE
            should_continue=false
          end

          all_mrs += mrs
        end
        all_mrs
        
      end
    end
  end
  
  module Gitlab
    class Client < API
      include Epics
      include MergeRequests
    end
  end
 