require 'pry'
require 'gitlab'
require 'pp'
require 'dotenv/load'
require_relative 'modules'
require 'logger'

def logger
    logger = Logger.new(STDOUT)
    logger.level = Logger::INFO
    logger
end

PER_PAGE = 100
PROJECT_ID = 9970
LABELS = "group::memory"
WEEKS_TO_Graph = 16

#TODO - Make this commandline so that you don't have to hard code epic IDs
EPICS = Array[1855, 1810]


Gitlab.configure do |config|
    config.endpoint = 'https://gitlab.com/api/v4'
    #puts 'Enter your private token'
    config.private_token = ENV["PAT"]
end

$all_mrs = []

def output_issues_csv(all_issues, filename="issues.csv")
    f_issues = File.open(filename, "w")
    f_issues.puts "ISSUE_IID|ISSUE_STATE|AUTHOR_ID|AUTHOR_USERNAME|ASSIGNEE|ISSUE_TITLE|ISSUE_CREATED_AT|ISSUE_CLOSED_AT|DUE_DATE|UPVOTES|WEB_URL"
    logger.info("all_issues.size #{all_issues.size}")
    all_issues.each do |issue|
        assignee_name = "not assigned"
        if issue.assignee && issue.assignee.to_h.count > 0
            assignee_name = issue.assignee.username
            
        end
        f_issues.puts "#{issue.iid}|#{issue.state}|#{issue.author.id}|#{issue.author.username}|#{assignee_name}|#{issue.title}|#{issue.created_at}|#{issue.closed_at}|#{issue.due_date}|#{issue.upvotes}|#{issue.web_url}"
    end

end

def output_mrs_csv(all_mrs, filename="mrs.csv", only_open=false)
    f_mrs = File.open(filename, "w")
    f_mrs.puts "MR_IID|STATE|AUTHOR_ID|DAYS_STALE|AUTHOR_USERNAME|ASSIGNEE|TITLE|LABELS|CREATED_AT|MERGED_AT|CLOSED_AT|UPDATED_AT|URL"
    all_mrs.each do |mr_arr|
        #for some reason this is returning an array
        mr_arr.each do |mr|
            assignee_name = "not assigned"
            days_stale = ( DateTime.now()-DateTime.parse(mr.updated_at)).to_i
            if mr.assignee && mr.assignee.to_h.count > 0
                assignee_name = mr.assignee.username
            end
            if mr.state == "opened" || !only_open
                f_mrs.puts "#{mr.iid}|#{mr.state}|#{mr.author.id}|#{days_stale}|#{assignee_name}|#{mr.author.username}|#{mr.title}|#{mr.labels}|#{mr.created_at}|#{mr.merged_at}|#{mr.closed_at}|#{mr.updated_at}|#{mr.web_url}"
            end
        end
    end
end

def get_issue_mrs(pid, issue_id)
    Gitlab.related(pid.to_i, issue_id.to_i)
end

def get_all_issues_mrs(all_issues)
    all_mrs = []
    all_issues.each do |issue|
        #all_mrs  << get_issue_mrs(issue.project_id, issue.iid)
        mrs = get_issue_mrs(issue.project_id, issue.iid)
        mrs.each do |mr|
            all_mrs << mr
        end
    end
    all_mrs
end

def get_child_epics(epiclist=[1810], group=9970)
    logger.info("get_child_epics epiclist=#{epiclist} group=#{group}")
    epiclist.each do |epic|
        childarray = []
        logger.info("Getting children for epic #{epic}")
        children = Gitlab.epic_children(epic, group)
        children.each do |child|
            logger.debug("Entering recursive loop for epic #{child}")
            epiclist.append(child.iid.to_s)
            childarray.append(child.iid)
            get_child_epics(child.iid.to_s.split(','), child.group_id)
        end
        #get_child_epics(childarray)
    end
    epiclist
end


def get_all_issues(epiclist=[1810], group=9970)
    all_issues = []
    logger.info("get_all_issues start with epiclist=#{epiclist} group=#{group}")
    epiclist.each do |epic|
        issues = Gitlab.all_epic_issues(epic, group)
        logger.info("Epic #{epic} issues count = #{issues.size}")
        issues.each do |issue|
            all_issues << issue
        end
    end
    all_issues
end 

def hash_to_file(hash, filename)
    File.open(filename, 'w') do |f| 
        hash.each do |k,v|
            f.puts "#{k} - #{v}"
        end
    end 
end

=begin
all_issues = get_all_issues()
output_issues_csv(all_issues)
all_mrs = get_all_issues_mrs(all_issues)
output_mrs_csv(all_mrs)
=end