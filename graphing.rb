require 'date'
require 'squid'
require_relative 'get_epic_issues'
require 'pp'
require 'docopt'
require 'logger'

def logger
    logger = Logger.new(STDOUT)
    logger.level = Logger::INFO
    logger
end

docstring = <<DOCSTRING
Create Issue and Merge Request burn up charts for the given epics.

Usage:
  #{__FILE__} --epic=<epic1,epic2,epic3> [--group=<9970>]  [--weeks=<n>] [--mrs=<yes | no>]
  #{__FILE__} -h | --help

Options:
  -h --help                 Show this screen.
  --epic=<epic1,epic2>      List the epics you would like to display in burn up charts 
  --group=<n>             The group where the epic resides [default: 9970]
  --week=<n>                Number of weeks to graph 
  --mrs=<yes/no>            Graph merge requests. This is slow, defaulting to no 
DOCSTRING

#Prawn::Document.generate 'web traffic.pdf' do
#    data = {views: {2013 => 182, 2014 => -46, 2015 => 88},
 #       uniques: {2013 => 104, 2014 => 27, 2015 => 14}}
#  chart data, type: :line, labels: [false, true], formats: %i(percentage percentage), line_widths: [0.5]
#end


weeks_to_graph = 6

options = Docopt::docopt(docstring)
epics = options.fetch('--epic')
group = options.fetch('--group')
weeks = options.fetch('--weeks').to_i
mrs = options.fetch('--mrs')
if weeks > 0
  weeks_to_graph = weeks.to_i
end

logger.info("Command line options/defaults epics=#{epics} group=#{group} weeks=#{weeks} mrs=#{mrs}")
#=begin

$week_array =[]
start_date = Date.today+1 #Date.parse('2019-10-10')
start_date = start_date - (weeks_to_graph*7)
$week_array << start_date

i=1
while i<=weeks_to_graph
  if weeks_to_graph < 4
    #do days of the week
    for j in 1..7
      $week_array << (start_date + ( i*j ))
    end
  else
      $week_array << (start_date + (i*7))
  end

    i+=1
end

def safe_parse(value, default = nil)
    Date.parse(value.to_s)
  rescue ArgumentError
    default
end

def graph_line(graph_data, title)
  Prawn::Document.generate title do
    data = graph_data
       chart data, type: :line, labels: [true, true], line_widths: [2, 2], colors: %w(fc6d26 6e49cb)
  end
end

def graph_issues(all_issues, epics)
  
  created_at = []
  closed_at = []

  all_issues.each do |issue|
      
          created = safe_parse(issue.created_at)
          created_at << created unless created.nil?
          closed = safe_parse(issue.closed_at)
          closed_at << closed unless closed.nil?
  end

  created_hash = Hash.new
  closed_hash = Hash.new

  $week_array.each do |week|
    created_hash[week] = created_at.count{|x| x <= week}
    closed_hash[week] = closed_at.count{|x| x <= week}
  end
  data = { Created: created_hash, Closed: closed_hash}
  graph_line(data, "Issues Burn Up for epics #{epics}.pdf")
end

def graph_mrs(all_issues, epics)
  all_mrs = get_all_issues_mrs(all_issues)
  created_at = []
  merged_at = []
  closed_at = []
  all_mrs.each do |mr|
          created = safe_parse(mr.created_at)
          created_at << created unless created.nil?
          merged = safe_parse(mr.merged_at)
          merged_at << merged unless merged.nil?
          closed = safe_parse(mr.closed_at)
          closed_at << closed unless closed.nil?
  end

  created_hash = Hash.new
  merged_hash = Hash.new
  closed_hash = Hash.new

  $week_array.each do |week|
    created_hash[week] = created_at.count{|x| x < week}
    merged_hash[week] = merged_at.count{|x| x < week} + closed_at.count{|x| x < week}
    closed_hash[week] = closed_at.count{|x| x < week}
  end
  data = {Created: created_hash, Merged_Closed: merged_hash}
  graph_line(data, "MR Burn Up for epics #{epics}.pdf")
end



###get_child_epics(epics.split(','))
#=begin
logger.info("Line 137 epic list=#{epics.split(',')} group=#{group}")
all_issues = get_all_issues(get_child_epics(epics.split(','), group), group)
graph_issues(all_issues, epics)
if mrs == 'yes'
  graph_mrs(all_issues, epics)
end
#y = all_issues.count { |x| x.milestone[0].title  == "Backlog" }
milestones = Hash.new(0)
labels = Hash.new(0)
assignees = Hash.new(0)
assignees_closed = Hash.new(0)
check_for_labels = ["workflow", "group", "querylimiting-disable", "severity", "Architecture", "won't"]
assigned = 0
open_issues_with_mr = 0
active_mrs = []
open_issues = []
issue_mrs = []
all_issues.each do |issue|
  if issue.milestone
    milestones[issue.milestone.title] += 1 
  end
  # Check to see if it is assigned
  if issue.assignees.count > 0
    assigned +=1
  end
  # Collect assignee counts
  issue.assignees.each do |assignee|
    if assignee and issue.state == "opened"
      assignees[assignee.name]+=1
    end
    if assignee and issue.state == "closed"
      assignees_closed[assignee.name]+=1
    end
  end

  if issue.state == "opened"
    # output issue info if no related MR
    # output MR data to CSV if MR still open
    issue_mrs = get_issue_mrs(issue.project_id, issue.iid)
    if issue_mrs.count > 0
      open_issues_with_mr +=1
      active_mrs << issue_mrs
    end
    open_issues << issue
  end

  # Collect labels to indicate group assignment and progress
  issue.labels.each do |label|
    #if label.include?("workflow") || label.include?("group") || label.include?("querylimiting-disable") || label.include?("severity") ||label.include?("won't") || label.include?("Architecture")
    if check_for_labels.any? { |l| label.include?(l)}
      labels[label] += 1
    end 
  end
 end
 hash_to_file(milestones, "milestones.txt")
 hash_to_file(labels.sort_by {|k, v| k}, "labels.txt")
 puts "Open  = #{all_issues.count {|x| x.state=="opened"}}"
 puts "Open and Assigned = #{all_issues.count {|x| x.assignees.count > 0 && x.state=="opened"}}"
 puts "Open with an MR = #{open_issues_with_mr}"
 hash_to_file(assignees.sort_by {|k, v| -v}, "open_assignees.txt")
 hash_to_file(assignees_closed.sort_by {|k, v| -v}, "closed_assignees.txt")
 puts "Writing open_issues.csv"
 output_issues_csv(open_issues, "open_issues.csv")
 puts "Writing active_mrs.csv"
 output_mrs_csv(active_mrs, "active_mrs.csv", true)
 puts "Writing all issues"
 output_issues_csv(all_issues, "all_issues.csv")
#=end

